# Load test script on Jmeter #

## Pre-conditions ##
1. Install Java locally. Link to Java: https://www.java.com/en/download/
2. Download Jmeter **version 5.2.1**. Link to binaries: https://archive.apache.org/dist/jmeter/binaries/. Search for version 5.2.1 within the downloads page.
3. Copy the jar `manager/jmeter-plugins-manager-1.4.jar` to folder: `<path to installed jmeter>/apache-jmeter-5.2.1/lib/ext`   

## How to Run ##
1. Copy the `EF.jmx` file and `data` folder into `<path to installed jmeter>/apache-jmeter-5.2.1/bin`
2. Run jmeter

   NOTE 1: To run JMeter, run the jmeter.bat (for Windows) or jmeter (for Unix) file. These files are found in the bin directory. After a short time, the JMeter GUI should appear.
   
   NOTE 2: GUI mode should only be used for creating the test script, CLI mode (NON GUI) must be used for load testing

3. Open EF.jmx file (In jMeter  File > Open)
4. You will be asked to install additional plugins which are required. Wait while jmeter reloads.
5. Review script
